Feature: Verifying 	Login details

  Scenario Outline: Verifying User is able to submit SIA Form
    Given User is on login page
    When Do login with "<userName>" and "<password>"
    And User Should click login button
    Then User Navigate to the SIA Form List Page
    And User clicks create SIA form
    Then User select the System - "<SystemName>"
    And Enter the ReleaseName - "<Release Name>"
    Then Select the Release Date
    And Click Next Button
    Then Fill All Question Tabs - "<Comments>"
    And Click Next Button
    Then User enables the confirmation toggle
    And User clicks the save and submit form.
    And Check success message

    Examples:
      | userName                               | password     | Release Name        | Release Details                   | SystemName           | Comments  |
      | bhuvaneswari.aravind@globalmantrai.com | Valholla@123 | Test125_{Timestamp} | Valholla Security Governance Tool | SIA_Automated_System | Test12234 |


  Scenario Outline: Verifying approver is able to Reject SIA from
    Given User is on login page
    When Do login with "<userName>" and "<password>"
    And User Should click login button
    Then User Navigate to the SIA Form List Page
    And User clicks the existing SIA form - "<SIA Form Release Name>"
    Then User Rejects the SIAForm - "<Comments>"

    Examples:
      | userName                                  | password    | SIA Form Release Name   | Comments |
      | bhuvaneswari.aravind@ventechsolutions.com | welcome@123 | Test125_{Timestamp}   | Test123456789  |

  Scenario Outline: Verifying User is able to Re-submit SIA Form
    Given User is on login page
    When Do login with "<userName>" and "<password>"
    And User Should click login button
    Then User Navigate to the SIA Form List Page
    And User clicks the existing SIA form - "<Release Name>"
    And Click Next Button
    Then Sleep for 1 seconds
    Then User Click Confirm And Submit Tab
    Then Sleep for 1 seconds
    Then User enables the confirmation toggle
    And User clicks the save and submit form.

    Examples:
      | userName                               | password     | Release Name        | Release Details                   | SystemName           | Comments  |
      | bhuvaneswari.aravind@globalmantrai.com | Valholla@123 | Test125_{Timestamp} | Valholla Security Governance Tool | SIA_Automated_System | Test12234 |

  Scenario Outline: Verifying approver is able to Approve SIA from
    Given User is on login page
    When Do login with "<userName>" and "<password>"
    And User Should click login button
    Then User Navigate to the SIA Form List Page
    And User clicks the existing SIA form - "<SIA Form Release Name>"
    Then User Approves the SIAForm - "<Comments>"

    Examples:
      | userName                                  | password    | SIA Form Release Name   | Comments |
      | bhuvaneswari.aravind@ventechsolutions.com | welcome@123 | Test125_{Timestamp}   | Test123456789  |