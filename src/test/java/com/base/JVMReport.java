package com.base;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;

public class JVMReport {
	public static void generateJVMReport(String jsonFile) {

		Configuration con = new Configuration(new File(System.getProperty("user.dir") + "/src/test/resources/Reports/"),
				"SIAFORM");
		con.addClassifications("BrowserName", "Chrome");
		con.addClassifications("Version", "80");
		con.addClassifications("Windows", "10");

		List<String> jsonFiles = new ArrayList<String>();
		jsonFiles.add(jsonFile);
		ReportBuilder builder = new ReportBuilder(jsonFiles, con);
		builder.generateReports();
	}

}
