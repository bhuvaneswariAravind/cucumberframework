package com.base;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LibGlobal {
	public static WebDriver driver;
	private static String stepLogs ="";
	
	public static final int ELEMENT_TIMEOUT = 15;
	public static final String Timestamp= String.valueOf(new Date().getTime());
	public static final boolean RunInRemote= true;
	 

	public void getDriver() {
		if (RunInRemote){
			try {
				driver = new RemoteWebDriver(new URL("http://3.6.202.184:4444/wd/hub"), DesiredCapabilities.chrome());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				throw new RuntimeException(e);
			}		
		}
		else {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		}
			
		
	 driver.manage().window().maximize();
	
	}
	

	public void loadUrl(String url) {
		driver.get(url);
	}

	public void type(WebElement element, String name) {
		name = name.replace("{Timestamp}", Timestamp);
		element.sendKeys(name);
		if(element!=null)
			addStepLogMessage("Value: '"+name+"' Entered in Element: "+element.toString());
	}
	
	public void type1(List<WebElement> element) {
		element.get(0).click();
	}
	
	

	public void typeJs(WebElement element, String name) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('value'," + name + ")", element);

	}

	public void btnClick(WebElement element) {
		element.click();
		if(element!=null)
			addStepLogMessage("Clicked Element: "+element.toString());
	}

	public void sleep() {
		try {
			Thread.sleep(3000);
			addStepLogMessage("sleep for 3 secs");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void sleep(long milisec) {
		try {
			Thread.sleep(milisec);
			addStepLogMessage("sleep for "+milisec+" millisecs");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void btnClick1(List<WebElement> element) {
		element.get(0).click();
	}



	public void scrollPageUpJS() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("$0.scrollIntoView(false);");
	}
	public void scrollUpJS(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(false)", element);
		if(element!=null)
			addStepLogMessage("Scroll Into View Element: "+element.toString());
	}
	public void scrollDownJS(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true)", element);
		if(element!=null)
			addStepLogMessage("Scroll Into View Element: "+element.toString());

	}

	public void attach() throws InterruptedException, AWTException {
		StringSelection strSel = new StringSelection("D:\\SanityTest.docx");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);

		
		Robot robot;
		robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);

		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	public void quitBrowser() {
		driver.quit();
	}

	public void btnClickJS(WebElement element) {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click()", element);
	}
	
	public void btnClickJS1(WebElement element) {

		element.click();
		System.err.println();
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		js.executeScript("arguments[0].checked=true;", element);
	}

	public void waitElementPresent(List<WebElement> elements) {

		for (int i = 0; i < 100; i++) {
			try {
				WebElement element = elements.get(0);
				boolean enabled = element.isEnabled();
				boolean b = element.isDisplayed();
				if (b && enabled) {
					break;
				}
			} catch (Throwable e) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

		}
	}
	
	public void waitElementPresent(WebElement element) {

			try {
				WebDriverWait wait = new WebDriverWait(driver, 5);
				wait.until(ExpectedConditions.visibilityOf(element));
				
			} catch (Throwable e) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
				
					e1.printStackTrace();
				}
			}

	}
	
	public WebElement getElementFromLocatorString(String locatorString)
	{
		
		locatorString = locatorString.replace("{Timestamp}", Timestamp);
		By by = getByLocatorFromString(locatorString);
		 WebElement ele = new WebDriverWait(driver, ELEMENT_TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(by));
		return ele;
	}
	
	public List<WebElement> getElementListFromLocatorString(String locatorString)
	{
		
		locatorString = locatorString.replace("{Timestamp}", Timestamp);
		By by = getByLocatorFromString(locatorString);
		 List<WebElement> eles = new WebDriverWait(driver, ELEMENT_TIMEOUT).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
		return eles;
	}
	
	public static  By getByLocatorFromString(String locatorStr)
	{
		int indexOfDelimit = locatorStr.indexOf(':');
		By by = null;
		if(indexOfDelimit>-1)
		{
			String locatorType = locatorStr.substring(0, locatorStr.indexOf(':'));
			String locatorValue = locatorStr.substring((locatorStr.indexOf(':')+1), locatorStr.length());
			switch (locatorType.toLowerCase()) {
				case "xpath":
					by = By.xpath(locatorValue);
				break;
				
				case "id":
					by = By.id(locatorValue);
				break;
				
				case "name":
					by = By.name(locatorValue);
				break;
	
				default:
					break;
			}
		}
		else
			throw new RuntimeException("Improper locator String - "+locatorStr);
		return by;
	}

	public static void addStepLogMessage(String logMessage)
	{
		stepLogs = stepLogs+new Date().toString()+"\tINFO\t"+logMessage+"\n";
	}

	public static String getStepLogs()
	{
		return stepLogs;
	}

	public static void clearStepLogs()
	{
		stepLogs = "";
	}
	
//public static void main(String[] args) {
//
//	System.out.println(getByLocatorFromString("xpath://input[@placeholder='Search']"));
//}
	public void getTitle() {
		String Title=driver.getTitle();
		System.out.println(Title);

	}
	
	
	public void dropDown(WebElement element, String value) {
		Select s= new Select(element);
		s.selectByVisibleText(value);
	}
	
	
	public void dragAndDrop(WebElement source,WebElement target) {
		Actions acc =new Actions(driver);
		acc.dragAndDrop(source, target);

	}
	
	
	public void mouseOver(WebElement element) {
		Actions acc =new Actions(driver);
		acc.moveToElement(element).perform();
	}
	
	
	public void doubleClick(WebElement element) {
		Actions acc =new Actions(driver);
		acc.doubleClick(element).perform();
	}
	
	
	public void rightClick(WebElement element) {
		Actions acc =new Actions(driver);
		acc.contextClick(element).perform();
	}
	
	public void radioButton(WebElement element) {
		element.click();

	}
	
	public void checkbox(WebElement element) {
		element.click();

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

