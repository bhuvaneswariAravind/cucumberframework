package com.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.base.LibGlobal;

public class LoginPage extends LibGlobal {
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}

	//locators
	private String txtUserName = "xpath://input[@formcontrolname='email']";
	private String txtpassword = "xpath://input[@formcontrolname='password']";
	private String btnLogin = "xpath://a[text()='Login']";
	private String verificationCode = "xpath://input[@formcontrolname='VerificationCode']";
	private String verify = "xpath://a[text()='Verify']";
	

	public WebElement getVerificationCode() {
		return getElementFromLocatorString(verificationCode);
	}

	public WebElement getTxtUserName() {
		return getElementFromLocatorString(txtUserName);
	}

	public WebElement getTxtpassword() {
		return getElementFromLocatorString(txtpassword);
	}

	public WebElement getBtnLogin() {
		return getElementFromLocatorString(btnLogin);
	}

	public WebElement getVerify() {
		return getElementFromLocatorString(verify);
	}

	
}
