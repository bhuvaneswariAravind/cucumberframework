package com.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.base.LibGlobal;

public class SIAApprover extends LibGlobal{
	public SIAApprover() {
		PageFactory.initElements(driver, this);
	}
	
	//locators
	private String search = "xpath://input[@placeholder='Search']";
	private String dynamicSIAFormLinkXpath = "xpath://u[@class='hyperlink'][text()='$$SIAFormName$$']";
	

	public WebElement getSearch() {
		return getElementFromLocatorString(search);
	}


	public WebElement getSiaformLink(String siaFormName) {				
		return getElementFromLocatorString(dynamicSIAFormLinkXpath.replace("$$SIAFormName$$", siaFormName));
	}
	

}
