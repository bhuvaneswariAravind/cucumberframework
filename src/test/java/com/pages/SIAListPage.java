package com.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.base.LibGlobal;

public class SIAListPage extends LibGlobal {
	public SIAListPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//a[text()=' Compliance ']")
	private List<WebElement> menu;
	
	@FindBy(xpath="//span[text()='SIA Form']")
	private List<WebElement> dropdown;
	
	@FindBy(xpath="(//input[@value='CREATE SIA FORM'])[2]")
	private List<WebElement> create;

	
	private  String listPageTitle = "xpath://div[@class='panel-heading'][text()='Security Impact Analysis']";

	public List<WebElement> getDropdown() {
		return dropdown;
	}


	public List<WebElement> getCreate() {
		return create;
	}

	public List<WebElement> getMenu() {
		return menu;
	}

	public WebElement getPageTitle()
	{
		return getElementFromLocatorString(listPageTitle);
	}
	
}
