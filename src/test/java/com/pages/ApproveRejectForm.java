package com.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.base.LibGlobal;

public class ApproveRejectForm extends LibGlobal{
	public ApproveRejectForm() {
		PageFactory.initElements(driver, this);
	}

	//locators
	private String reject = "id:btnRejectSIAForms";
	private String rejectComments = "id:rejectComment";
	private String rejectConfirm = "xpath://button[contains(text(), ' Confirm')]";
	private String approve = "id:btnApproveSIAForms";

	public WebElement getReject() {
		return getElementFromLocatorString(reject);
	}
	
	public WebElement getRejectComments() {
		return getElementFromLocatorString(rejectComments);
	}
	
	public WebElement getRejectConfirm() {
		return getElementFromLocatorString(rejectConfirm);
	}
	
	public WebElement getApprove() {
		return getElementFromLocatorString(approve);
	}
	
}
