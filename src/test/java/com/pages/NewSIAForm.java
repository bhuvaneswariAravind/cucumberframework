package com.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.base.LibGlobal;

public class NewSIAForm extends LibGlobal {
	public NewSIAForm() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//mat-select[@name='System Name']")
	private WebElement systemName;

	@FindBy(xpath = "//input[@id='releaseName']")
	private WebElement releaseName;

	@FindBy(xpath = "//span[text()=' Automated System ']")
	private WebElement dropdown;

	@FindBy(xpath = "//mat-datepicker-toggle//button[@class='mat-icon-button mat-button-base']")
	private List<WebElement> releaseDate;

	@FindBy(css = ".mat-calendar-body-cell-content.mat-calendar-body-today")
	private List<WebElement> btnDate;

	@FindBy(xpath = "//span[contains(text(),'database')]")
	private List<WebElement> scrollDown;

	
	private String btnNext ="id:btnNextTab";
	
	@FindBy(xpath="//div[@class='angular-editor-textarea']")
	private List<WebElement> releaseDetails;
	
	public List<WebElement> getReleaseDetails() {
		return releaseDetails;
	}

	public WebElement getBtnNext() {
		return getElementFromLocatorString(btnNext);
	}

	public List<WebElement> getBtnDate() {
		return btnDate;
	}

	public WebElement getDropdown() {
		return dropdown;
	}

	public List<WebElement> getReleaseDate() {
		return releaseDate;
	}

	public List<WebElement> getScrollDown() {
		return scrollDown;
	}

	public WebElement getSystemName() {
		return systemName;
	}

	public WebElement getReleaseName() {
		return releaseName;
	}
	
	public WebElement getDynamicSystemOption(String systemName)
	{
		return driver.findElement(By.xpath("//mat-option//span[@class='mat-option-text'][normalize-space(text())='"+systemName+"']"));
	}
	

}
