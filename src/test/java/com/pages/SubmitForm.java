package com.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.base.LibGlobal;

public class SubmitForm extends LibGlobal {
	public SubmitForm() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//span[@class='toggle']")
	private WebElement toggle; 
	
	
	@FindBy(id="btnSubmitSIAForms")
	private WebElement submit; 
	
	@FindBy(id="btnSaveSIAForms")
	private WebElement save;
	
	@FindBy(xpath="//button[@class='btn btn-default']")
	private WebElement cancel;
	

	public WebElement getCancel() {
		return cancel;
	}

	public WebElement getToggle() {
		return toggle;
	}

	public WebElement getSubmit() {
		return submit;
	}

	public WebElement getSave() {
		return save;
	} 
	
	
	
}
