package com.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.base.LibGlobal;

public class SecurityPage extends LibGlobal {

	public SecurityPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "(//span[@class='toggle'])[1]")
	private WebElement toggle;

	@FindBy(xpath = "//button[@class='btn btn-sm mat-raised-button btn-attach mat-button-base']")
	private List<WebElement> btnAttach;

	@FindBy(xpath  = "(//div[@class='angular-editor-textarea'])[1]")
	private List<WebElement> txtEditor;

	@FindBy(xpath = "//input[@name='fileName']")
	private List<WebElement> btnChoose;

	@FindBy(xpath =   "//button[@class='btn btn-success btn-s']")
	private List<WebElement> btnUploadAll;

	@FindBy(xpath = "(//button[contains(text(),'Cancel')])[2]")
	private List<WebElement> btnCancel;
	
	@FindBy(id = "btnNextTab")
	private List<WebElement> Next;

	//Tabs
	private String allSIAFormTabs = "xpath://div[@class='mat-tab-labels']/div";
	private String dynamicSIAFormTab = "xpath:(//div[@class='mat-tab-labels']/div)[$$TabIndex$$]";
	private String confirmAndSubmitTab = "xpath://div[@class='mat-tab-labels']/div[normalize-space(.)='Confirm and Submit']";

	//QA Sections
	private String allQuestionSectionsInCurrentTab = "xpath://app-sia-question-detail/div";
	private String dynamicQASectionAnswer_ToggleBtn = "xpath://app-sia-question-detail/div[$$qaSectionIndex$$]//div[@class='togglebutton']//span[@class='toggle']";
	private String dynamicQASectionAttachFile_Btn = "xpath://app-sia-question-detail/div[$$qaSectionIndex$$]//button[@title='Attach supporting documents']";
	private String dynamicQASectionComments_txtArea = "xpath://app-sia-question-detail/div[$$qaSectionIndex$$]//div[@class='angular-editor-textarea']";


	public List<WebElement> getAllSIAFormTabs() {
		return getElementListFromLocatorString(allSIAFormTabs);
	}

	public WebElement getSIATabBasedOnIndex(int index) {
		return getElementFromLocatorString(dynamicSIAFormTab.replace("$$TabIndex$$", index+""));
	}

	public WebElement getConfirmAndSubmitTab() {
		return getElementFromLocatorString(confirmAndSubmitTab);
	}


	public List<WebElement> getAllQuestionSectionsInCurrentTab() {
		return getElementListFromLocatorString(allQuestionSectionsInCurrentTab);
	}

	public WebElement getAnswerToggleBtnBasedOnIndex(int index) {
		return getElementFromLocatorString(dynamicQASectionAnswer_ToggleBtn.replace("$$qaSectionIndex$$", index+""));
	}

	public WebElement getAttachBtnBasedOnIndex(int index) {
		return getElementFromLocatorString(dynamicQASectionAttachFile_Btn.replace("$$qaSectionIndex$$", index+""));
	}

	public WebElement getCommentsTxtAreaBasedOnIndex(int index) {
		return getElementFromLocatorString(dynamicQASectionComments_txtArea.replace("$$qaSectionIndex$$", index+""));
	}



	public List<WebElement> getNext() {
		return Next;
	}

	public WebElement getToggle() {
		return toggle;
	}

	public List<WebElement> getBtnCancel() {
		return btnCancel;
	}

	public List<WebElement> getBtnUploadAll() {
		return btnUploadAll;
	}

	public List<WebElement> getBtnChoose() {
		return btnChoose;
	}

	public List<WebElement> getBtnAttach() {
		return btnAttach;
	}

	public List<WebElement> getTxtEditor() {
		return txtEditor;
	}

}
