package com.runner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.base.JVMReport;
import com.base.LibGlobal;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = { "src/test/resources/" }, glue = { "com.stepdefinition" }, monochrome = true, plugin = {
		"pretty", "json:src/test/resources/Reports/cucumber.json" }, strict=true)
public class TestRunnerClass {

	@BeforeClass
	public static void BeforeClass() {
		System.out.println("'{Timestamp}' will be replaced with '"+LibGlobal.Timestamp+"'");

	}
	@AfterClass
	public static void AfterClass() {
		JVMReport.generateJVMReport(System.getProperty("user.dir") + "/src/test/resources/Reports/cucumber.json");
		System.out.println("'{Timestamp}' will be replaced with '"+LibGlobal.Timestamp+"'");

	}


}
