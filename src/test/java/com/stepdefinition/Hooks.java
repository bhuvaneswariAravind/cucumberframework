package com.stepdefinition;

import cucumber.api.java.AfterStep;
import cucumber.api.java.BeforeStep;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.base.LibGlobal;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends LibGlobal {

	@Before
	public void beforeScenario() {
		getDriver();
		loadUrl("https://valholla-test.vsolgmi.com/");

	}

	@BeforeStep
	public void beforeStep(Scenario sc) {
		clearStepLogs();
	}

	@AfterStep
	public void afterStep(Scenario sc) {
		if(!getStepLogs().isEmpty())
			sc.embed(LibGlobal.getStepLogs().getBytes(), "text/plain");

		TakesScreenshot tk = (TakesScreenshot) driver;
		byte[] b = tk.getScreenshotAs(OutputType.BYTES);
		sc.embed(b, "image/png");
	}

	@After
	public void afterScenario(Scenario sc) {
		TakesScreenshot tk = (TakesScreenshot) driver;
		byte[] b = tk.getScreenshotAs(OutputType.BYTES);
		sc.embed(b, "image/png");

	}

}
