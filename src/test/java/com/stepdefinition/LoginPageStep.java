package com.stepdefinition;

import java.awt.AWTException;

import org.openqa.selenium.interactions.Actions;

import com.base.LibGlobal;
import com.pages.SIAListPage;
import com.pages.LoginPage;
import com.pages.NewSIAForm;
import com.pages.ApproveRejectForm;
import com.pages.SIAApprover;
import com.pages.SecurityPage;
import com.pages.SubmitForm;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginPageStep extends LibGlobal {
	LoginPage loginPage;
	SIAListPage siaListpage;
	NewSIAForm newSiaFormpage;
	SecurityPage securityPage;
	SubmitForm submitForm;
	SIAApprover siaApprover;
	ApproveRejectForm approveRejectForm;	
	
	@Given("User is on login page")
	public void user_is_on_login_page() {
	}

	@When("Do login with {string} and {string}")
	public void user_enters_and(String name, String password) {
		loginPage = new LoginPage();

		type(loginPage.getTxtUserName(), name);
		type(loginPage.getTxtpassword(), password);

	}

	@When("User Should click login button")
	public void user_Should_click_login_button() {
		btnClick(loginPage.getBtnLogin());
		waitElementPresent(loginPage.getVerificationCode());
		type(loginPage.getVerificationCode(), "123456");
		btnClickJS(loginPage.getVerify());

	}

	@Then("User Navigate to the SIA Form List Page")
	public void user_clicks_the_SIA_form_in_Complaince() throws InterruptedException {
		siaListpage = new SIAListPage();
		waitElementPresent(siaListpage.getMenu());
		Actions acc = new Actions(driver);
		acc.moveToElement(siaListpage.getMenu().get(0)).perform();
		waitElementPresent(siaListpage.getDropdown());
		btnClick1(siaListpage.getDropdown());

	}

	@Then("User clicks create SIA form")
	public void user_clicks_create_SIA_form() {

		waitElementPresent(siaListpage.getCreate());
		btnClickJS(siaListpage.getCreate().get(0));

	}

	@Then("User select the System - {string}")
	public void user_clicks_the_System_Name(String sysName) throws AWTException {
		newSiaFormpage = new NewSIAForm();
		
//		waitElementPresent(page2.getScrollDown());
//		scrollDownJS(page2.getScrollDown().get(0));
		waitElementPresent(newSiaFormpage.getSystemName());
		newSiaFormpage.sleep(1000);
		scrollUpJS(newSiaFormpage.getSystemName());
		btnClick(newSiaFormpage.getSystemName());
		newSiaFormpage.sleep(1000);
		btnClick(newSiaFormpage.getDynamicSystemOption(sysName));
	}

	@Then("Enter the ReleaseName - {string}")
	public void enterRelease(String release) {
		type(newSiaFormpage.getReleaseName(), release);
	}


	
	
	@Then("Select the Release Date")
	public void user_clicks_the_Release_Date() throws InterruptedException, AWTException {
		btnClickJS(newSiaFormpage.getReleaseDate().get(0));
		btnClickJS(newSiaFormpage.getBtnDate().get(0));
		sleep(1);
	}

	@Then("Click Next Button")
	public void user_clicks_next() throws InterruptedException, AWTException {
		newSiaFormpage = new NewSIAForm();
		scrollDownJS(newSiaFormpage.getBtnNext());		
		btnClickJS(newSiaFormpage.getBtnNext());
	}

	@Then("User Click Confirm And Submit Tab")
	public void user_clicks_confirm_and_submit_tab() throws InterruptedException, AWTException {
		securityPage = new SecurityPage();
		scrollUpJS(securityPage.getConfirmAndSubmitTab());
		btnClickJS(securityPage.getConfirmAndSubmitTab());
	}




	@Then("Fill All Question Tabs - {string}")
	public void fillAllQuestionTabs(String comments) {

		securityPage = new SecurityPage();
		int allTabCount = securityPage.getAllSIAFormTabs().size();
		addStepLogMessage("Total Tab Size: "+allTabCount);
		for(int tabIndex=2;tabIndex<allTabCount;tabIndex++)
		{
			addStepLogMessage("Current TabIndex: "+tabIndex);
			scrollUpJS(securityPage.getSIATabBasedOnIndex(tabIndex));
			securityPage.getSIATabBasedOnIndex(tabIndex).click();
			securityPage.sleep(1000);
			int allQASecCountInCurrTab = securityPage.getAllQuestionSectionsInCurrentTab().size();
			addStepLogMessage("QA Section Count: "+allQASecCountInCurrTab);
			for(int qaSecIndex=1;qaSecIndex<allQASecCountInCurrTab+1;qaSecIndex++)
			{
				addStepLogMessage("Current QaSectionIndex: "+qaSecIndex);
				user_enables_the_QA_toggle(qaSecIndex);
				attach_file(qaSecIndex);
				enterComment(qaSecIndex, comments);
			}
		}
	}

	@Then("User enables the question-answer toggle {int}")
	public void user_enables_the_QA_toggle(int qaSecIndex) {

		securityPage = new SecurityPage();
//		securityPage.sleep(2000);
		btnClickJS1(securityPage.getAnswerToggleBtnBasedOnIndex(qaSecIndex));
	}

		@Then("Attach file {int}")
		public void attach_file(int qaSecIndex) {
			
			securityPage.getAttachBtnBasedOnIndex(qaSecIndex).click();
			securityPage.sleep(1000);
			if(LibGlobal.RunInRemote) {
				securityPage.getBtnChoose().get(0).sendKeys("C:\\Users\\Administrator\\Documents\\test.png");
			}
			else {
				securityPage.getBtnChoose().get(0).sendKeys("C:\\Users\\abhuvaneswari\\Documents\\text.docx");
			}
			
			securityPage.sleep(1000);
			securityPage.getBtnUploadAll().get(0).click();
			securityPage.sleep(3000);
			securityPage.getBtnCancel().get(0).click();
			securityPage.sleep(1000);
		}
		
		@Then("Users enters the Comments - {int} {string}")
		public void enterComment(int qaSecIndex, String comments) {
			type(securityPage.getCommentsTxtAreaBasedOnIndex(qaSecIndex), comments);
		}
		
		@Then("Sleep for {int} seconds")
		public void sleep(int sec) {
			submitForm = new SubmitForm();			
			submitForm.sleep(sec*1000);
		}
		
		@Then("User enables the confirmation toggle")
		public void user_enables_the_COnfirmation_toggle() {
			
			submitForm = new SubmitForm();			
			submitForm.sleep(2000);
			btnClickJS1(submitForm.getToggle());
		}
		
		@Then("User clicks the save and submit form.")
		public void user_clicks_the_save_and_submit_form() {
			submitForm = new SubmitForm();
			btnClickJS1(submitForm.getSubmit());
		}
			@Then("Check success message")
			public void check_success_message() {
				
			waitElementPresent(siaListpage.getPageTitle());
			 // System.out.println("Successfully Saved and Submitted");
			//driver.navigate().to("https://valholla-test.vsolgmi.com/");
			}
		
		
		
		@Then("User clicks the existing SIA form - {string}")
		public void user_clicks_the_SIA_form_created(String siaFormName) {
			siaApprover=new SIAApprover();
			type(siaApprover.getSearch(), siaFormName);
			btnClick(siaApprover.getSiaformLink(siaFormName));
			
		}
			

		@Then("User Rejects the SIAForm - {string}")
		public void user_Rejects_the_SIAForm(String comments) {
			approveRejectForm=new ApproveRejectForm();
			scrollDownJS(approveRejectForm.getReject());
		    btnClickJS(approveRejectForm.getReject());
		    type(approveRejectForm.getRejectComments(), comments);
		    btnClickJS(approveRejectForm.getRejectConfirm());		    
		    
		}
	
		@Then("User Approves the SIAForm - {string}")
		public void user_Approves_the_SIAForm(String comments) {
			approveRejectForm=new ApproveRejectForm();
			scrollDownJS(approveRejectForm.getApprove());
		    btnClickJS(approveRejectForm.getApprove());	    
		    
		}
		

}

